<h1>Assignment 2: Frontend Development with React  </h1>

<h2> Table of contects </h2>

- Background
- Install
- Usage
- Maintainers
- License
- Authors and acknowledgment


<h2> Background </h2>
In this assignment we created a sign language translator. you can login, translate words to sign language and see the last 10 translations (only the words not the sign language) in your profile page. 
 

<h2> Install </h2>

- NPM/Node.js (LTS – Long Term Support version)
- React CRA (create-react-app)
- Visual Studio Code Text Editor/ IntelliJ
- Redux Dev Tools

<h2> Usage </h2>
At the first page you can login by typing a username. after pressing the button you will be taking to the translation page where you can translate words to sign language. If you press the profile icon at the top right corner you will be taking the the profil page. Here you can see your history, delete your history or logout and return to the login page.   


<h2> Maintainers </h2>
@trygvejohannessen88, @BilleLind and @bartomen

<h2> License </h2>
The project is open-source. Feel free to use it in your own projects, as long as credit the work.

<h2> Authors and Acknowledgmen </h2>
Code author: Anders Bille Lind, Trygve Johannessen, Bart van Dongen.

Assignment given by: Piotr Dziubinski, Lecturer at Noroff University College.
