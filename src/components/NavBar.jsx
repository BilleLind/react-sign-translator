import { NavLink } from 'react-router-dom'
import '../styles/components/NavBar.css'

const NavBar = ({ children }) => {
	return (
		<nav>
			<div>
        <img src={"./hello-sky.png"} alt="Friendly robot saying hello" />

        <NavLink
         to="/translation">
          <h3 className=''>Lost in Translation</h3>
			  </NavLink>

      </div>
      <div>
        {children}
      </div>
		</nav>
	)
}
export default NavBar
