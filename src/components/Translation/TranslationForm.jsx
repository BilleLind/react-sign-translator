import React from "react";
import { useForm } from "react-hook-form";

const TranslationForm = ({translate}) => {
    const { register, handleSubmit } = useForm()

    const submit = (input) => {
        translate(input)
    };

	return (
        <form onSubmit={ handleSubmit(submit) } className="input-box">
            <svg width="50" height="50" viewBox="0 0 24 24" className="black input-box-key">
                <path fill="currentColor" d="M3 21q-.825 0-1.412-.587Q1 19.825 1 19V6q0-.825.588-1.412Q2.175 4 3 4h18q.825 0 1.413.588Q23 5.175 23 6v13q0 .825-.587 1.413Q21.825 21 21 21Zm0-2h18V6H3v13Zm5-2h8v-1H8Zm-3-3h2v-2H5Zm4 0h2v-2H9Zm4 0h2v-2h-2Zm4 0h2v-2h-2ZM5 10h2V8H5Zm4 0h2V8H9Zm4 0h2V8h-2Zm4 0h2V8h-2ZM3 19V6v13Z" />
            </svg>

            <input className="w-full" type="text" placeholder="Write something" { ...register("input") }  />

            <button className="btn-center" type="submit">
                <svg width="70" height="70" viewBox="0 0 24 24" className="arrow">
                    <path fill="currentColor" d="M22 12c0-5.52-4.48-10-10-10S2 6.48 2 12s4.48 10 10 10s10-4.48 10-10zM4 12c0-4.42 3.58-8 8-8s8 3.58 8 8s-3.58 8-8 8s-8-3.58-8-8zm12 0l-4 4l-1.41-1.41L12.17 13H8v-2h4.17l-1.59-1.59L12 8l4 4z" />
                </svg>
            </button>
        </form>
    )
};
export default TranslationForm
