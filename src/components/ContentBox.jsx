import '../styles/components/ContentBox.css'

const contentBox = ({ box, bottom }) => {
	return (
		<>
			<div className="box">{box}</div>
			<div className="purple-bottom">{bottom}</div>
		</>
	)
}
export default contentBox
