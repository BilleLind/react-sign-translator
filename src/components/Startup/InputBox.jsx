import '../../styles/components/Startup/InputBox.css'

import { useForm } from 'react-hook-form'
import { useEffect, useState } from 'react'

import { loginUser } from '../../api/user'
import { useNavigate } from 'react-router-dom'
import { useUser } from '../../context/UserContext'

const usernameConfig = {
	required: true,
	minLength: 4,
}

const InputBox = () => {
	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm()
	const { user, setUser } = useUser()
	const [isLoading, setLoading] = useState(false)
	let navigate = useNavigate()

	useEffect(() => {
		if (user !== null) {
			navigate('/translation')
		}
	}, [user, navigate])

	const onSubmit = async ({ username }) => {
		setLoading(true)
		const { error, user } = await loginUser(username)
		console.log(error)
		if (user !== null) {
			setUser(user)
		}
		setLoading(false)
	}

	const errorMessage = (() => {
		if (!errors.username) {
			return null
		}
		if (errors.username.type === 'required') {
			return <span style={{color: 'red'}}>Username is required</span>
		}
		if (errors.username.type === 'minLength') {
			return <span style={{color: 'red'}}>Username is too short</span>
		}
	})()
	return (
		<>
			<form onSubmit={handleSubmit(onSubmit)} className="input-box">
				<svg width="32" height="32" viewBox="0 0 24 24" className="black input-box-key">
					<path
						fill="currentColor"
						d="M3 21q-.825 0-1.412-.587Q1 19.825 1 19V6q0-.825.588-1.412Q2.175 4 3 4h18q.825 0 1.413.588Q23 5.175 23 6v13q0 .825-.587 1.413Q21.825 21 21 21Zm0-2h18V6H3v13Zm5-2h8v-1H8Zm-3-3h2v-2H5Zm4 0h2v-2H9Zm4 0h2v-2h-2Zm4 0h2v-2h-2ZM5 10h2V8H5Zm4 0h2V8H9Zm4 0h2V8h-2Zm4 0h2V8h-2ZM3 19V6v13Z"
					/>
				</svg>
				<input className="w-full" type="text" {...register('username', usernameConfig)} placeholder="What's your name?" />
				<button onSubmit={handleSubmit(onSubmit)} className="btn-center" type="submit" disabled={isLoading}>
					<svg width="32" height="32" viewBox="0 0 24 24" className="arrow">
						<path
							fill="currentColor"
							d="M22 12c0-5.52-4.48-10-10-10S2 6.48 2 12s4.48 10 10 10s10-4.48 10-10zM4 12c0-4.42 3.58-8 8-8s8 3.58 8 8s-3.58 8-8 8s-8-3.58-8-8zm12 0l-4 4l-1.41-1.41L12.17 13H8v-2h4.17l-1.59-1.59L12 8l4 4z"
						/>
					</svg>
				</button>
			</form>
			{errorMessage}
			{isLoading && <span>Loading....</span>}
		</>
	)
}

export default InputBox
