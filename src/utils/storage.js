import { useState } from 'react'
/* export const storageSave = (key, value) => {
	localStorage.setItem(key, JSON.stringify(value))
}

export const storageRead = (key) => {
	const data = localStorage.getItem(key)
	if (data) {
		return JSON.parse(data)
	}
	return null
}

export const storageDelete = (key) => {
	localStorage.removeItem(key)
} */

export const useLocalStorage = (keyName, defaultValue) => {
	const [storedValue, setStoredValue] = useState(() => {
		try {
			const value = window.localStorage.getItem(keyName)

			if (value) {
				return JSON.parse(value)
			} else {
				window.localStorage.setItem(keyName, JSON.stringify(defaultValue))
				return defaultValue
			}
		} catch (err) {
			return defaultValue
		}
	})

	const setValue = (newValue) => {
		try {
			window.localStorage.setItem(keyName, JSON.stringify(newValue))
		} catch (err) {}
		setStoredValue(newValue)
	}

	return [storedValue, setValue]
}
