import React from 'react';
import ReactDOM from 'react-dom/client';
import {
	Routes,
	Route,
	BrowserRouter, // used when configuring with jsx
	//Link, // used to link to each route eg. <Link to="profile">text<Link/>
} from 'react-router-dom'

// Routes
import Startup from './views/Startup';
import Translation from './views/Translation';
import Profile from './views/Profile';

//Styles
import './styles/index.css';

//Components
import AppContext from './context/AppContext';


import reportWebVitals from './reportWebVitals';



/* const router = createBrowserRouter([
	{
		path: '/',
		element: <Startup />,
	},
	{
		path: '/translation',
		element: <Translation />,
	},
	{
		path: '/profile',
		element: <Profile />,
	},
]) */

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
	<React.StrictMode>
		<AppContext>
			{/* <RouterProvider router={router} /> */}
			<BrowserRouter>
				<Routes>
					<Route path="/" element={<Startup />} />
					<Route path="/translation" element={<Translation />} />
					<Route path="/profile" element={<Profile />} />
				</Routes>
			</BrowserRouter>
		</AppContext>
	</React.StrictMode>
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
