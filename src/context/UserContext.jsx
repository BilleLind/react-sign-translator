import { createContext, useContext } from 'react'
import { STORAGE_KEY_USER } from '../constants/StorageKeys'
import {  useLocalStorage } from '../utils/storage'

const UserContext = createContext()

export const useUser = () => {
	return useContext(UserContext)
}

const UserProvider = ({ children }) => {
	const [user, setUser] = useLocalStorage(STORAGE_KEY_USER, null)

	const state = {
		user,
		setUser,
	}
	return <UserContext.Provider value={state}>{children}</UserContext.Provider>
}
export default UserProvider
