import { createHeaders } from './utility'

const API_URL = process.env.REACT_APP_API_URL

const checkForUser = async (username) => {
	try {
		const response = await fetch(`${API_URL}translations?username=${username}`)

		if (!response.ok) {
			throw new Error('Could not complete request')
		}
		const user = await response.json()
		
		return {error: null, user}
	} catch (error) {
		return {error:error.message, user:[]}
	}
}

const createUser = async (username) => {
	try {
		const response = await fetch(`${API_URL}translations`, {
			method: 'POST',
			headers: createHeaders(),
			body: JSON.stringify({
				username,
				translations: [],
			}),
		})
		if (!response.ok) {
			throw new Error('Could not create user with username' + username)
		}
		const user = await response.json()
		return {error:null, user}
	} catch (error) {
		return {error:error.message, user:[]}
	}
}

export const loginUser = async (username) => {
	const {error, user} = await checkForUser(username)

	if (error !== null) {
		return {error, user:[]}
	}

	if (user.length > 0) {
		return {error:null, user:user.pop()}
	}
	return await createUser(username)
}

export const deleteUser = async (userId) => {
    try {
        const response = await fetch(`${API_URL}translations/${userId}`, {
            method: 'DELETE',
            headers: createHeaders()
        })
        if (!response.ok) {
            throw new Error('Could not delete user with userId ' + userId)
        }
        const data = await response.json()
        return [null, data]
    }
    catch (error) {
        return [ error.message,null ]
    }
}

export const userById = async (userId) => {
    try {
        const response = await fetch(`${API_URL}translations/${userId}`)
        if (!response.ok) {
            throw new Error('Could not fetch user')
        }
        const user = await response.json()
        return [null, user]
    }
    catch (error) {
        return [ error.message, null ]
    }
}