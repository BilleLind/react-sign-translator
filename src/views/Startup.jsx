import '../styles/Startup.css'

import ContentBox from '../components/ContentBox'
import InputBox from '../components/Startup/InputBox'
import NavBar from '../components/NavBar'


const Startup = () => {
	return (
		<>
		<NavBar>
			
		</NavBar>
		<main>
			<div className="startup-upper">
				<div className="startup-upper-box">
					<img src={'./hello-sky.png'} alt="Friendly Robot saying Hello" />
					<div>
						<h1>Lost in Translation</h1>
						<p>Get Started</p>
					</div>
				</div>
			</div>
			<div className="startup-lower">
				<ContentBox
					box={
						<InputBox input="username"/>
					}
				/>
			</div>
		</main>
		</>
	)
}
export default Startup
