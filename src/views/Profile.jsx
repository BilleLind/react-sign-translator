import '../styles/Profile.css'
import { useUser } from '../context/UserContext'
import { NavLink, useNavigate } from 'react-router-dom'
import NavBar from '../components/NavBar'
import { Logout } from '../components/Profile/LogOutBtn'
import { useForm } from 'react-hook-form'
import ContentBox from '../components/ContentBox'
import { translationRemove } from '../api/translation'
import widthAuth from '../hoc/widthAut'

const TranslationListItem = ({ translation }) => {
	return <li>{translation}</li>
}

const Profile = () => {
	const { user, setUser } = useUser()
	var list = (user.translations.length > 10? user.translations.splice(user.translations.length-10): user.translations)
	
	const translationList = list.map((translation, index) => <TranslationListItem key={index} translation={translation} />)
	const { handleSubmit } = useForm()
	const navigate = useNavigate()

	const logOut = () => {
		setUser(null)
		navigate('/')
	}

	const clear = async () => {
		console.log("clear")
		const [error, updatedUser] = await translationRemove(user)
		if (error === null) {
			setUser(updatedUser)
		}
	}

	return (
		<>
			<NavBar>
				<NavLink to="/profile">
					<h5 className="nav-name-heading">{user.username}</h5>
				</NavLink>

				<form onSubmit={handleSubmit(logOut)} className="nav-svg-bg flex-center" title="Logout">
					<Logout style={{ cursor: 'pointer' }}></Logout>
				</form>
			</NavBar>
			<main>
				<div className="yellow">
					<div className="container">
						<h1>Translations</h1>
						<h3>Last 10</h3>
					</div>
					<div className="overlap">
						<img className="Robot-Profile" src={'./logo-hello.png'} alt="Friendly Robot saying Hello" />
					</div>
					<ContentBox box={<p> {translationList} </p>} bottom={<button onClick={clear}>Clear</button>} />
				</div>
			</main>
		</>
	)
}

export default widthAuth(Profile)
