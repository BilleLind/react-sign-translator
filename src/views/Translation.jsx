import React, { useState } from "react";
import { NavLink } from "react-router-dom"
import { translationAdd } from "../api/translation";
import { useUser } from "../context/UserContext";
import ContentBox from '../components/ContentBox'
import NavBar from "../components/NavBar";
import TranslationForm from "../components/Translation/TranslationForm";
import { UserProfile } from "../components/Translation/user";
import '../styles/Translation.css'
import widthAuth from "../hoc/widthAut";

const HandSign = ({letter}) => {
    return (
        <img src ={`./individial_signs/${letter}.png`} alt="sign"></img>
    )
};

const Translation = () => {
    const [ handSignList, setHandSignList ] = useState([]);
    const { user, setUser } = useUser()
    
    const translate = async (input) => {
        const inputString = input.input
        const array = inputString.split("")
        const handSignList = array.map((letter, index) => <HandSign letter={letter} key={index} /> )
        setHandSignList(handSignList)
        //save to api
        const [error, updatedUser] = await translationAdd(user,inputString) 
        if (error !== null) {
            return
        }
        setUser(updatedUser)
    };

	return (
		<main>
			<NavBar>
				<NavLink to="/profile"><h5 className="nav-name-heading">{ user.username }</h5></NavLink>
				<NavLink to="/profile"> <UserProfile className="nav-svg-bg"/> </NavLink>
			</NavBar>
	
			<div className="translation-upper">
				<div className="translation-upper-box">	
				<TranslationForm translate={ translate }></TranslationForm>
				</div>
			</div>
			
			<div className="translation-lower">
				<ContentBox
					box={<p>{ handSignList }</p>}
					bottom={<span>Translation</span>}
				/>
			</div>
		</main>
    )
};
export default widthAuth(Translation)
